window.Journal = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},

  initialize: function(posts, content, sidebar) {
    var posts = new Journal.Collections.Posts(posts);
    this._sidebar(sidebar, posts);
    new Journal.Routers.Posts(content, sidebar, posts);
    Backbone.history.start();
  },

  _sidebar: function(sidebar, posts) {
    var indexView = new Journal.Views.PostsIndex({
      collection: posts
    })
    sidebar.html(indexView.render().$el);
  }

};

