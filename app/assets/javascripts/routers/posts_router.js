Journal.Routers.Posts = Backbone.Router.extend({
  initialize: function(content, sidebar, posts) {
    this.posts = posts;
    this.content = $(content);
    this.sidebar = $(sidebar);
  },

  routes: {
    '': 'index',
    'posts/new': 'edit',
    'posts/:id': 'show',
    'posts/:id/edit': 'edit'
  },

  index: function() {
    this.content.html();
  },

  show: function(id) {
    var that = this;
    var post = that.posts.get(id);
    var showView = new Journal.Views.ShowView({
      model: post
    });
    that.content.html(showView.render().$el);
  },

  edit: function(id){
    var that = this;
    var post = that.posts.get(id);
    var editView = new Journal.Views.PostEdit({
      collection: that.posts,
      model: post
    });
    that.content.html(editView.render().$el);
  }


});
