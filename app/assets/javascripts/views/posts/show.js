Journal.Views.ShowView = Backbone.View.extend({

  initialize: function(){},

  events: {
    "dblclick #post-title": "editTitle",
    "dblclick #post-body" : "editBody",
    "blur input" : "submit"
  },

  template: JST['posts/show'],

  render: function(){
    var that = this;
    var rendered = that.template({
      post: that.model
    });
    that.$el.html(rendered);
    return that;
  },

  editTitle: function(event){
    var title = this.model.get('title');
    var editTitle = $(event.currentTarget).html("<input type='text' name='title' value='" + title + "'>");
  },

  editBody: function(event){
    var body = this.model.get('body');
    var editBody = $(event.currentTarget).html("<input type='text' name='body' value='" + body + "'>");
  },

  submit: function(event){
    var editedAttr = $(event.currentTarget).attr("name");
    var newVal = $(event.currentTarget).val();
    this.model.set(editedAttr, newVal);
    this.model.save();
    this.render();
  }

})