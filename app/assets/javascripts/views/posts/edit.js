Journal.Views.PostEdit = Backbone.View.extend({
  initialize: function() {
    this.model = (this.model || new Journal.Models.Post());
  },

  template: JST['posts/edit'],

  events: {
    "submit .post-edit-form" : "submit"
  },

  render: function(){
    var that = this;
    var rendered = that.template({
      post: that.model
    });
    that.$el.html(rendered);
    return that;
  },

  submit: function(event){
    var that = this;

    event.preventDefault();
    var postData = $(event.currentTarget).serializeJSON();
    var editedPost = that.model.set(postData.post);



    editedPost.save(that.model.attributes, {
      success: function(post){
        that.collection.add(post);
        Backbone.history.navigate("#/posts/" + that.model.id);
      },
      error: function(model, xhr, options){
        that.$el.prepend("<p>Errors: " + xhr.responseText + "</p>");
      }
    });


  }
});
