Journal.Views.PostsIndex = Backbone.View.extend({
  initialize: function() {
    var that = this;
    this.listenTo(that.collection, "all", that.render.bind(that));
    this.listenTo(that.collection, "add", that.render.bind(that));
    this.listenTo(that.collection, "change:title",  that.render.bind(that));
    this.listenTo(that.collection, "remove",  that.render.bind(that));
    this.listenTo(that.collection, "reset",  that.render.bind(that));
  },

  template: JST['posts/index'],

  events: {
    "click button.delete-button": "destroy"
  },

  render: function() {
    var that = this;
    var rendered = that.template({
      posts: that.collection
    })
    that.$el.html(rendered);
    return that;
  },

  destroy: function(event) {
    event.preventDefault();
    var postId = ($(event.currentTarget).attr("data-id"));
    var post = this.collection.get(postId);
    post.destroy();
  }

});
